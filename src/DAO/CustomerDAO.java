package DAO;

import POJO.Customer;
public interface CustomerDAO {
	
	boolean userLogin(String userID, String password);
	void insertRecord(Customer refCustomer);
	void userLogOut();
}
