package service;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;
import DAO.CustomerDAO;
import DAO.customerDAOImplementation;
import POJO.Customer;

public class CustomerServiceImplementation implements CustomerService {
	
	CustomerDAO refCustomerDAO;
	Scanner scannerRef;
	Customer refCustomer;
	
	@Override 
	public void userChoice() {
		System.out.print("Enter user id: ");
		scannerRef = new Scanner(System.in);
		String userID = scannerRef.next();
		
		System.out.print("Enter Password: ");
		String password = scannerRef.next();
		
		refCustomerDAO = new customerDAOImplementation();
		boolean result = refCustomerDAO.userLogin(userID, password);
		
		while(!result) {
			System.out.println("Login fail");
			System.out.println("Please try again");
			
			System.out.print("Enter user id: ");
			scannerRef = new Scanner(System.in);
			userID = scannerRef.next();
			
			System.out.print("Enter Password: ");
			password = scannerRef.next();
			
			refCustomerDAO = new customerDAOImplementation();
			result = refCustomerDAO.userLogin(userID, password);
			
		}
		
		System.out.println("Login Successful");	
		int choice = 0;
		boolean validInput = false;
		do {
			System.out.println("Please select what you want to accomplish today");
			System.out.println("Choice 1: Add a new customer");
			System.out.println("Choice 2: Logout");
			try {
				validInput = false;
				choice = scannerRef.nextInt();
				validInput = true;
				switch(choice) {
					case 1: 
						System.out.println("Enter new customer userID");
						userID = scannerRef.next();
						System.out.println("Enter new customer password");
						password = scannerRef.next();
						Customer refCustomer = new Customer(userID,password);
						refCustomerDAO.insertRecord(refCustomer);
						System.out.println("Record Inserted Sucessfully");
						break;
					case 2: 
						refCustomerDAO.userLogOut();
				}
			} catch(InputMismatchException e) {
				System.out.println("Invalid Input please try again");
			}
		} while(choice != 2);
		
		
	
	}
}
